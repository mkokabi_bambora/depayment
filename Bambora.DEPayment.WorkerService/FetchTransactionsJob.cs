﻿using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Threading.Tasks;

namespace Bambora.DEPayment.WorkerService
{
    [DisallowConcurrentExecution]
    public class FetchTransactionsJob : IJob
    {
        private readonly ILogger<FetchTransactionsJob> logger;

        public FetchTransactionsJob(ILogger<FetchTransactionsJob> logger)
        {
            this.logger = logger;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            logger.LogInformation($"Fetch started at {DateTime.Now}");
            await Task.Delay(2000);
            logger.LogInformation($"Fetch completed at {DateTime.Now}");
        }
    }
}
