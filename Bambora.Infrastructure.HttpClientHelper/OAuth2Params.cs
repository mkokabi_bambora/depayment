﻿namespace Bambora.Infrastructure.HttpClientHelper
{
    public class OAuth2Params
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string RefreshTokenUrl { get; set; }
        public string RefreshToken { get; set; }
    }
}