﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Bambora.Infrastructure.HttpClientHelper
{
    internal class AccessTokenInjecter : DelegatingHandler
    {
        private readonly HttpClientHelper httpClientHelper;

        public AccessTokenInjecter(HttpClientHelper httpClientHelper)
        {
            this.httpClientHelper = httpClientHelper;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (!request.Headers.Contains("Authorization"))
            {
                var accessToken = await httpClientHelper.ExchangeRefreshForAccessToken();
                request.Headers.Add("Authorization", $"Bearer {accessToken}");
            }
            var response = await base.SendAsync(request, cancellationToken);
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                var accessToken = await httpClientHelper.ExchangeRefreshForAccessToken();
                if (request.Headers.Contains("Authorization"))
                {
                    request.Headers.Remove("Authorization");
                }
                request.Headers.Add("Authorization", $"Bearer {accessToken}");
                response = await base.SendAsync(request, cancellationToken);
            }
            return response;
        }
    }
}