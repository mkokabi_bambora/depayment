﻿using Bambora.Infrastructure.ParameterManager;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Bambora.Infrastructure.HttpClientHelper
{
    public class HttpClientHelper
    {
        private readonly IHttpClientFactory httpClientFactory;
        private readonly OAuth2Params oAuth2Params;
        private readonly IParameterWriter parameterWriter;
        private string previousRefreshToken;

        public HttpClientHelper(IHttpClientFactory httpClientFactory, OAuth2Params oAuth2Params, IParameterWriter parameterWriter)
        {
            this.httpClientFactory = httpClientFactory;
            this.oAuth2Params = oAuth2Params;
            this.parameterWriter = parameterWriter;
        }

        public async Task<string> ExchangeRefreshForAccessToken()
        {
            var nameValueCollection = new List<KeyValuePair<string, string>> {
                new KeyValuePair<string, string>("refresh_token", previousRefreshToken ?? oAuth2Params.RefreshToken),
                new KeyValuePair<string, string>("client_id", oAuth2Params.ClientId ),
                new KeyValuePair<string, string>("client_secret", oAuth2Params.ClientSecret ),
                new KeyValuePair<string, string>("grant_type", "refresh_token")
            };

            HttpClient httpClient = httpClientFactory.CreateClient();

            var response = await httpClient.PostAsync(oAuth2Params.RefreshTokenUrl,
                new FormUrlEncodedContent(nameValueCollection));

            var responseContent = await response.Content.ReadAsStringAsync();

            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new HttpRequestException($"Failed to retrieve the refresh token. Response: {responseContent}");
            }
            var resfreshTokenResponse = JsonSerializer.Deserialize<RefreshTokenResponse>(responseContent);
            previousRefreshToken = resfreshTokenResponse.RefreshToken;
            parameterWriter.WriteParameter("/OAuthParams/RefreshToken", previousRefreshToken);

            return resfreshTokenResponse.AccessToken;
        }

        public class RefreshTokenResponse
        {
            [JsonPropertyName("access_token")]
            public string AccessToken { get; set; }
            [JsonPropertyName("refresh_token")]
            public string RefreshToken { get; set; }
        }
    }
}
