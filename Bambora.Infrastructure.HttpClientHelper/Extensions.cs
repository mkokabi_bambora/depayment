﻿using Bambora.Infrastructure.ParameterManager;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Bambora.Infrastructure.HttpClientHelper
{
    public static class Extensions
    {
        public static void RegisterHttpClient(this IServiceCollection services, string clientName, OAuth2Params oAuth2Params)
        {
            services.AddSingleton(oAuth2Params);
            services.AddSingleton<HttpClientHelper>();
            services.AddTransient<AccessTokenInjecter>();

            services.AddHttpClient(clientName)
                .AddHttpMessageHandler<AccessTokenInjecter>();
        }

        public static void RegisterHttpClient(this IServiceCollection services, string clientName, string keysPath)
        {
            services.AddSingleton((ctx) =>
            {
                var parameterReader = ctx.GetService<IParameterReader>();
                var parameters = parameterReader.GetKeysByPath(keysPath).Result;
                var config = ctx.GetService<IConfiguration>();
                var oAuth2Params = new OAuth2Params
                {
                    ClientId = parameters["/OAuthParams/ClientId"],
                    ClientSecret = parameters["/OAuthParams/ClientSecret"],
                    RefreshToken = parameters["/OAuthParams/RefreshToken"],
                    RefreshTokenUrl = config["SplitPaymentRefreshTokenUrl"]
                };
                return oAuth2Params;
            });
            services.AddSingleton<HttpClientHelper>();
            services.AddTransient<AccessTokenInjecter>();

            services.AddHttpClient(clientName)
                .AddHttpMessageHandler<AccessTokenInjecter>();
        }
    }
}
