﻿namespace Bambora.Infrastructure.ParameterManager
{
    public interface IParameterWriter
    {
        void WriteParameter(string key, string value);
    }
}
