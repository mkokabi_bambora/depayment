﻿using System.Collections.Generic;
using System.IO;

namespace Bambora.Infrastructure.ParameterManager
{
    public class InFileParameterWriter : IParameterWriter
    {
        private string fileName;

        public InFileParameterWriter(string fileName)
        {
            this.fileName = fileName;
        }

        public void WriteParameter(string key, string value)
        {
            var lines = new List<string>();
            if (File.Exists(fileName))
            {
                lines.AddRange(File.ReadAllLines(fileName));
            }
            lines.Add($@"""{ key }""=""{value}""");
            File.WriteAllLines(fileName, lines);
        }
    }
}
