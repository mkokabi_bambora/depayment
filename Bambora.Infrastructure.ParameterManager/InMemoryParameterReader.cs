﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bambora.Infrastructure.ParameterManager
{
    public class InMemoryParameterReader : IParameterReader
    {
        private Dictionary<string, string> keyValues;

        public InMemoryParameterReader(Dictionary<string, string> keyValues)
        {
            this.keyValues = keyValues;
        }

        public Task<Dictionary<string, string>> GetKeysByPath(string path)
        {
            var result = new Dictionary<string, string>();
            foreach (var key in keyValues.Keys)
            {
                if (key.StartsWith(path))
                {
                    result.Add(key, keyValues[key]);
                }
            }
            return Task.FromResult(result);
        }
    }
}