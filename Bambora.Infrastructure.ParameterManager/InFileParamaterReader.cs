﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Bambora.Infrastructure.ParameterManager
{
    public class InFileParamaterReader : IParameterReader
    {
        private readonly Dictionary<string, string> keyValues = new Dictionary<string, string>();

        public InFileParamaterReader(string fileName)
        {
            if (File.Exists(fileName))
            {
                var lines = File.ReadAllLines(fileName);
                var kvs = lines.Select(line =>
                {
                    var parts = line.Split(new string[] { @"""=""" }, System.StringSplitOptions.RemoveEmptyEntries);
                    var key = parts[0].TrimStart('"');
                    var value = parts[1].TrimEnd('"');
                    return new KeyValuePair<string, string>(key, value);
                }).ToList();
                kvs.ForEach(kv => keyValues[kv.Key] = kv.Value);
            }
        }

        public Task<Dictionary<string, string>> GetKeysByPath(string path)
        {
            var result = new Dictionary<string, string>();
            foreach (var key in keyValues.Keys)
            {
                if (key.StartsWith(path))
                {
                    result.Add(key, keyValues[key]);
                }
            }
            return Task.FromResult(result);
        }
    }
}
