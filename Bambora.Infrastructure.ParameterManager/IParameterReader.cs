﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bambora.Infrastructure.ParameterManager
{
    public interface IParameterReader
    {
        Task<Dictionary<string, string>> GetKeysByPath(string path);
    }
}
