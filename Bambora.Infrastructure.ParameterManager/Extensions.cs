﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bambora.Infrastructure.ParameterManager
{
    public static class Extensions
    {
        public static IParameterManager RegisterParameterManager(this IServiceCollection services)
        {
            return new ParameterManager(services);
        }

        public static IParameterManager UseInMemoryKeyParameterStore(this IParameterManager parameterManager, Dictionary<string, string> keyValues)
        {
            parameterManager.Services.AddSingleton<IParameterReader>(new InMemoryParameterReader(keyValues));
            return parameterManager;
        }

        public static IParameterManager UseInFileKeyParameterStore(this IParameterManager parameterManager, string fileName)
        {
            parameterManager.Services.AddSingleton<IParameterWriter>(new InFileParameterWriter(fileName));
            parameterManager.Services.AddSingleton<IParameterReader>(new InFileParamaterReader(fileName));
            return parameterManager;
        }
    }

    public interface IParameterManager 
    {
        IServiceCollection Services { get; }
    };

    public class ParameterManager : IParameterManager 
    {

        public ParameterManager(IServiceCollection services)
        {
            Services = services;
        }

        public IServiceCollection Services { get; }
    }
}
