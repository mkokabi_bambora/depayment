﻿using Bambora.Infrastructure.ParameterManager;
using Microsoft.Extensions.DependencyInjection;

namespace Bambora.Infrastructure.AwsParameterManager
{
    public static class Extensions
    {
        public static void UseAwsKeyParameterStore(this IParameterManager parameterManager)
        {
            parameterManager.Services.AddSingleton<IParameterReader>();
        }
    }
}
