﻿using Bambora.DEPayment.Services.Contracts;
using Bambora.DEPayment.Services.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Bambora.DEPayment.Services
{
    public static class Extensions
    {
        public static IServiceCollection RegisterDEPaymentService(this IServiceCollection services)
        {
            services.AddTransient<IAccountManagerService, AccountManagerService>();
            return services;
        }
    }
}
