﻿using Bambora.DEPayment.Services.Contracts;
using Bambora.DEPayment.Services.Models;
using Microsoft.Extensions.Configuration;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace Bambora.DEPayment.Services.Services
{
    public class AccountManagerService : IAccountManagerService
    {
        private readonly IHttpClientFactory httpClientFactory;
        private readonly IConfiguration configuration;

        public AccountManagerService(IHttpClientFactory httpClientFactory, IConfiguration configuration)
        {
            this.httpClientFactory = httpClientFactory;
            this.configuration = configuration;
        }

        public async Task<string> Sync()
        {
            var httpClient = httpClientFactory.CreateClient(Constants.SplitPaymentClientName);
            var response = await httpClient.GetAsync($"{configuration["SplitPaymentApiUrl"]}/{ Constants.GetBankAccountsAPI }");
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                var accountDetails = JsonSerializer.Deserialize<AccountDetails>(responseContent);
                return $"OK {accountDetails.Data.Count}";
            }

            return "Failed";
        }
    }
}
