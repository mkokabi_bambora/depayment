﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Bambora.DEPayment.Services.Models
{
    public class AccountDetails
    {
        [JsonPropertyName("data")]
        public List<AccountDetail> Data { get; set; }
    }

}
