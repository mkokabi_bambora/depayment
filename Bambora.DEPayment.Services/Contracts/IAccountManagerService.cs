﻿using System.Threading.Tasks;

namespace Bambora.DEPayment.Services.Contracts
{
    public interface IAccountManagerService
    {
        Task<string> Sync();
    }
}
