﻿namespace Bambora.DEPayment.Services
{
    public static class Constants
    {
        public const string SplitPaymentClientName = "SplitPayment";
        public const string GetBankAccountsAPI = "bank_accounts";
    }
}
