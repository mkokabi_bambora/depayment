﻿using Bambora.DEPayment.Services;
using Bambora.DEPayment.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Bambora.DEPayment.AccountManager.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MerchantsController : ControllerBase
    {
        private readonly IAccountManagerService accountManagerService;

        public MerchantsController(IAccountManagerService accountManagerService)
        {
            this.accountManagerService = accountManagerService;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status508LoopDetected)]
        public async Task<string> Sync()
        {
            return await accountManagerService.Sync();
        }
    }
}
